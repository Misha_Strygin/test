package Ex2;

import java.util.Arrays;
import java.util.Scanner;
public class Ex2 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
       int count = readCount();
       double[] array = new double[count];
       for(int i = 0; i < count; i++) {
           array[i] = Math.round(Math.random() * 10000.0) / 100.0;
       }
       
       int position = readPosition(count);
       System.out.printf("Массив до увеличения: %s%n", Arrays.toString(array));
       increase(array, position);
       System.out.printf("Массив после увеличения: %s%n", Arrays.toString(array));
    }
    public static int readCount() {
       System.out.print("Введите количество элементов массива: ");
       int count = scanner.nextInt();
       while(count < 0 || count > 1000) {
           System.out.print("Похоже, вы допустили ошибку при вводе числа. Попробуйте еще раз: ");
           count = scanner.nextInt();
       }
       return count;
    }
    public static int readPosition(int count) {
       System.out.print("Введите номер увеличиваемого элемента: ");
       int pos = scanner.nextInt();
       while(pos < 0 || pos >= count) {
           System.out.print("Похоже, вы допустили ошибку при вводе числа. Попробуйте еще раз: ");
           pos = scanner.nextInt();
       }
       return pos;
    }
    public static void increase(double[] array, int pos) throws UnsupportedOperationException {
       if(pos >= array.length)
           throw new UnsupportedOperationException("Уважаемый ва выходите за длину массива! Длина массива: " + array.length + ", индекс: " + pos);
       
       array[pos] *= 1.1;
   }
}    