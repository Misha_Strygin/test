package Ex3;

import java.util.Scanner;

public class Ex3 {     
    static Scanner scanner = new Scanner(System.in);
	
    public static void main(String[] args) {
        System.out.println("Введите курс евро по отношению к рублю (сколько рублей стоит 1 евро): ");
	double course = readCorrect();
	System.out.print("Введите количество рублей: ");
	double roubles = readCorrect();
	System.out.println("За"+ roubles +" рублей можно купить "+ (roubles / course) +" евро");
    }
  
    public static double readCorrect() {
	double v = scanner.nextDouble();
	while(v < 0) {
	    System.out.print("Допущена ошибка попробуйте ещё раз: ");
	    v = scanner.nextDouble();
        }
	return v;
    }