package Ex4;

import java.util.Scanner;

public class Ex4 {
    private static final double SPEED_OF_SOUND = 343.0;

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
	System.out.print("Введите время между молнией и громом в секундах: ");
	double time = readCorrect();
	System.out.println("За время "+ time +" сек. молния прошла "+ (time * SPEED_OF_SOUND) +" м%n");
    }

    public static double readCorrect() {
	double v = scanner.nextDouble();
	while(v < 0) {
	    System.out.print("Похоже что вы допустили ошибку, введите данные повторно: ");
	    v = scanner.nextDouble();
        }
	return v;
    }
}